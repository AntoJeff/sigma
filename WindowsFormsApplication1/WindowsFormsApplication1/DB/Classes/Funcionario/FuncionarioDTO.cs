﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Classes.Funcionario
{
    class FuncionarioDTO
    {
        public int id_funcionario { get; set; }
        public string usuario { get; set; }
        public string  senha { get; set; }
        public string  nome { get; set; }
        public DateTime data_nascimento { get; set; }
        public string cpf { get; set; }
        public int rg { get; set; }
        public string  endereço { get; set; }
        public string completo { get; set; }
        public string cargo { get; set; }
        public string  observacao { get; set; }


    }
}
