﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Classes.Funcionario
{
    class FuncionarioDatabase
    { 
        public int Salvar(FuncionarioDTO dto)
        {
            string script = @"INSERT INTO tb_funcionario(usuario,senha,nome,data_nascimento,cpf,rg,endereço,completo,cargo,observação) 
VALUES(@usuario,@senha,@nome,@data_nascimento,@cpf,@rg,@endereço,@completo,@cargo,@observação)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("usuario",dto.usuario));
            parms.Add(new MySqlParameter("senha",dto.senha));
            parms.Add(new MySqlParameter("nome",dto.nome));
            parms.Add(new MySqlParameter("data_nascimento",dto.data_nascimento));
            parms.Add(new MySqlParameter("cpf",dto.cpf));
            parms.Add(new MySqlParameter("rg",dto.rg));
            parms.Add(new MySqlParameter("endereço",dto.endereço));
            parms.Add(new MySqlParameter("completo",dto.completo));
            parms.Add(new MySqlParameter("cargo",dto.cargo));
            parms.Add(new MySqlParameter("observação",dto.observacao));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);



        } 
        public List<FuncionarioDTO> Listar()
        {
            string script = @"select * from tb_veiculo";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms); 
            List<FuncionarioDTO> listar = new List<FuncionarioDTO>(); 
            while(reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.id_funcionario = reader.GetInt32("id_funcionario");
                dto.usuario = reader.GetString("usuario");
                dto.senha = reader.GetString("senha");
                dto.nome = reader.GetString("nome");
                dto.data_nascimento = reader.GetDateTime("data_nascimento");
                dto.cpf = reader.GetString("cpf");
                dto.rg = reader.GetInt32("rg");
                dto.endereço = reader.GetString("endereco");
                dto.completo = reader.GetString("completo");
                dto.cargo = reader.GetString("cargo");
                dto.observacao = reader.GetString("observacao");
                listar.Add(dto);
            }
            reader.Close();
            return listar;
        }
    }
}
