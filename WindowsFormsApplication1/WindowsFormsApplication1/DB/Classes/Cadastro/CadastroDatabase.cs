﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Classes.Cadastro
{
    class CadastroDatabase
    { 
        public int salvar(CadastroDTO dto)
        {
            string script = @"INSERT INTO tb_cliente(cpf_cnpj,razao_social,rg,nome,data_nascimento,cep,estado,cidade,bairro,endereço,telefone,celular) 
VALUES (@cpf_cnpj,@razao_social,@rg,@nome,@data_nascimento,@cep,@estado,@cidade,@bairro,@endereço,@telefone,@celular)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("cpf_cnpj", dto.cpf_cnpj));
            parms.Add(new MySqlParameter("razao_social", dto.razao_social));
            parms.Add(new MySqlParameter("rg", dto.rg));
            parms.Add(new MySqlParameter("nome", dto.nome));
            parms.Add(new MySqlParameter("data_nascimento", dto.data_nasc));
            parms.Add(new MySqlParameter("cep", dto.celular));
            parms.Add(new MySqlParameter("estado", dto.estado));
            parms.Add(new MySqlParameter("cidade", dto.cidade));
            parms.Add(new MySqlParameter("bairro", dto.bairro));
            parms.Add(new MySqlParameter("endereço", dto.endereço));
            parms.Add(new MySqlParameter("telefone", dto.telefone));
            parms.Add(new MySqlParameter("celular", dto.celular));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);


        }  
        public List<CadastroDTO> Listar()
        {
            string script = @"select * from tb_cadastro";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms); 

        }
        
    }
}
