﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Classes
{
    class CadastroDTO
    {
        public int id_cliente { get; set; } 
        public int cpf_cnpj { get; set; }
        public string razao_social { get; set; }
        public string rg { get; set; }
        public string nome { get; set; }
        public DateTime data_nasc { get; set; }
        public int cep { get; set; }
        public string estado { get; set; }
        public string  cidade { get; set; }
        public string endereço { get; set; }
        public string bairro { get; set; }
        public int telefone { get; set; }
        public int celular { get; set; }

    }
}
